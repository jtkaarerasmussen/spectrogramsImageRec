import os
import tensorflow as tf
from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append(os.path.abspath("~/Documents/inter/specpy"))
from specpy import formating as ft
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--wavPath", help="The file path for the wav file.", default=None)
parser.add_argument("-s", help="hide Spectrogram", action="store_true")
args = parser.parse_args()
if args.wavPath is None:
    testPath = input('Input Path:')
else:
    testPath = args.wavPath

labels = np.array(['go', 'yes', 'down', 'right', 'up', 'left', 'no', 'stop'])

sampleRate, wav = ft.wavfile.read(testPath)
wav = wav[:16000]
wav = ft.zeroPad(wav, 16000 - len(wav))
spec = ft.getSpectrogram(wav, 255, 6, 128, 220)

if not args.s:
    plt.imshow(spec, origin='lower', aspect='auto', extent=[0, 8000, 0, 16000])
    plt.show()

print(f'spec shape: {np.shape(spec)}')

maxValue = np.amax(spec)
minValue = np.min(spec)
spec = (spec - minValue) / (maxValue - minValue)
spec = np.array([spec])


model = tf.keras.models.load_model('models/9')
prec = model(spec, training=False)
print(f'prediction: {labels[np.argmax(prec)]}  -  confidence: {np.amax(prec)/np.sum(prec)}')

# data/mini_speech_commands/stop/b0f24c9b_nohash_1.wav





















