import numpy as np
from matplotlib import pyplot as plt


def zeroPad(arr, addedLength, aliment='left', negAddedLengthReturnLength=12000):
    if addedLength >= 0:
        if aliment == 'right':
            out = np.zeros(int(len(arr) + addedLength))
            for i in range(len(arr)):
                out[-i - 1] = arr[-i - 1]

        elif aliment == 'left':
            out = np.zeros(int(len(arr) + addedLength))
            for i in range(len(arr)):
                out[i] = arr[i]

        elif aliment == 'center':
            out = zeroPad(arr, addedLength / 2, aliment='right')
            for i in range(int(addedLength / 2)):
                out = np.append(out, 0)
    else:
        out = np.zeros(negAddedLengthReturnLength)
        for i in range(len(out)):
            out[i] = arr[i]
    return out


def getMag(inputArray):
    if type(inputArray).__name__ != 'ndarray':
        inputArray = np.array([inputArray])

    # creates an empty list with the same shape as the input
    magnitude = np.empty(inputArray.shape)
    # interates through the input list converting to a real magnitude
    for i in range(inputArray.size):
        magnitude[i] = (np.sqrt(inputArray[i].real ** 2 + inputArray[i].imag ** 2)) / len(inputArray)
    # outputs magnitude
    return magnitude


def extend(arr, repLen):
    out = np.zeros(len(arr) + (repLen * 2))
    for i in range(len(arr)):
        out[i + repLen] = arr[i]
    for i in range(repLen):
        out[len(arr) + i + repLen] = arr[-i - 2]
        out[i] = arr[repLen - i]
    return out


def getTopHalf(inputArray):
    # create empty list with half the length of the input list
    outArray = np.empty(int(inputArray.size / 2))
    # Increment through the list doubling everything
    for i in range(int(inputArray.size / 2)):
        outArray[i] = inputArray[int(i + (inputArray.size / 2))]
    # output new list
    return outArray


def getGaussian(gaussianWidth, staDevCount=8, platformLength=0, staDev=1):
    out = np.arange(staDevCount * -0.5 * staDev, staDevCount * 0.5 * staDev, staDevCount * staDev / gaussianWidth)
    for x in range(gaussianWidth):
        out[x] = np.exp((-1 * (out[x] ** 2)) / (2 * (staDev ** 2)))

    if platformLength != 0:
        platformOut = np.ones(len(out) + platformLength)
        for i in range(int(gaussianWidth / 2)):
            platformOut[i] = out[i]
            platformOut[-i - 1] = out[-i - 1]
        return platformOut
    else:
        return out


def getSpectrogram(audioArray, winSize, staDevCount, shiftSize, platformLength=0, padLength=0):
    gaussian = getGaussian(winSize - platformLength, staDevCount, platformLength=platformLength)
    newAudioArray = extend(audioArray, int(winSize / 2))
    specImg = []
    for winCount in range(int(len(audioArray) / shiftSize)):
        windowArray = np.zeros(winSize)
        for sample in range(winSize):
            windowArray[sample] = newAudioArray[sample + (winCount * shiftSize)]
        windowArray = zeroPad(windowArray * gaussian, padLength, aliment='center')
        specImg.append(getTopHalf(getMag(np.fft.fftshift(np.fft.fft(windowArray)))))
    return np.swapaxes(np.array(specImg), 0, 1)


# x0 = np.arange(0, 1, 0.001)
# y0 = np.sin(x0 * np.pi * 2 * 200) + np.sin(x0 * np.pi * 2 * 300)
# x1 = np.arange(0, 128, 1)
# y1 = y0[:128]
#
# fig, axs = plt.subplots(3)
# axs[0].set_title('300 Hz and 200 Hz Sampled at 1000 Hz')
# axs[0].plot(x0, y0)
# axs[1].set_title('First 128 samples')
# axs[1].plot(x1, y1)
# axs[2].set_title('FFT of the First 128 samples')
# axs[2].plot(np.arange(0, 500, 1), getTopHalf(getMag(np.fft.fftshift(np.fft.fft(y0)))))
# plt.tight_layout()
# plt.show()


# x = np.arange(0, 1, 0.001)
# y1 = np.sin(x**2 * np.pi * 3 * 500)
# spec = getSpectrogram(y1, 128, 6, 10, 126, padLength=128)
# plt.title('Spectrogram of a Wave with a Frequency Increasing Linearly over Time')
# plt.xlabel('Time in seconds')
# plt.ylabel('Hz')
# plt.imshow(spec, origin='lower', aspect='auto', extent=[0, 1, 0, 500])
# plt.show()


# x = np.arange(0, 1, 1/128)
# x2 = np.arange(0, 2, 1/128)
# y = np.cos(x2 * np.pi * 2 * 10)
# s = zeroPad(getGaussian(40, platformLength=88), 128, aliment='center')
# fig, axs = plt.subplots(3)
# axs[0].plot(x2, y)
# axs[1].plot(x2, s)
# axs[2].plot(x2, y * s)
# plt.show()


# inPath = '/home/jonatank/Documents/inter/spec3/training'
# rows, cols = 2, 4
# examples = []
# wordList = ['go', 'go', 'stop', 'stop', 'left', 'left', 'right', 'right', 'no', 'no', 'yes', 'yes', 'up', 'up', 'down', 'down']
# numList = [104, 101, 103, 105, 100, 101, 100, 101, 106, 102, 100, 101, 100, 101, 100, 101]
# fig, axs = plt.subplots(nrows=2, ncols=4, figsize=(12, 6))
# for x in range(rows):
#     for y in range(cols):
#         loaded = np.load(f'{inPath}/{wordList[y + (x * 4)+8]}/{numList[y + (x * 4)+8]}.npy')
#         axs[x, y].imshow(loaded, origin='lower', aspect='auto', extent=[0, 16000, 0, 8000])
#         axs[x, y].set_title(wordList[y + (x * 4)+8])
# plt.tight_layout()
# plt.show()
#
# loaded = np.load('/home/jonatank/Documents/inter/spec3/training/stop/100.npy')
# fig, axs = plt.subplots(2)
# axs[0].plot(loaded)
# axs[1].imshow(spec, origin='lower', aspect='auto', extent=[0, 8000, 0, 16000])


x = np.arange(0, 1, 1 / 8)
f = np.arange(0, 4, 1)
w = np.sin(x * np.pi * 2 * 3)
w1 = np.sin(x * np.pi * 2 * 5)

x1 = np.arange(0, 1, 1/128)
y1 = np.sin(x1 * np.pi * 3 * 2)
y2 = -1 * np.sin(x1 * np.pi * 5 * 2)
# fig, axs = plt.subplots(nrows=2, ncols=2)
# axs[0,0].plot(x, w)
# axs[0,1].plot(x, w1)
# axs[0,0].plot(x1, y1)
# axs[0,1].plot(x1, y2)
# axs[1,0].plot(getTopHalf(getMag(np.fft.fftshift(np.fft.fft(y1)))))
# axs[1,1].plot(getTopHalf(getMag(np.fft.fftshift(np.fft.fft(y2)))))
plt.plot(x, w)
plt.plot(x1, y1)
plt.plot(x1, y2)
plt.legend(['3Hz at 8Hz', '3Hz', '5Hz'], loc='lower center')


plt.show()








