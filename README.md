# spectrogramsImageRec

This a project that uses the mini_speech_commands dataset to train a tensorflow model on spectrograms

mini_speech_commands contains 8 words (go, yes, down, right, up, left, no, stop)

formating.py creates spectrograms using the FFT

main.py creates and trains the model

everything else are helper or test scripts

The models folder are model checkpoints that where saved between training attempts

To get model's prediction run tester.py and input the path to a .wav file (only uses first 1 second and needs to be sampled at 16000 Hz.)

To do this paste:
```
$ pip3 install tensorflow numpy matplotlib scipy
```
and
```
$ python3 tester.py --wavPath example1.wav -s
```

tester.py needs:
os
tensorflow
tensorflow.keras
numpy
matplotlib.pyplot
sys
specpy.py (in specpy folder)


