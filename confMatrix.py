import tensorflow as tf
from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import os

labels = np.array(['go', 'yes', 'down', 'right', 'up', 'left', 'no', 'stop'])

testingPath = '/home/jonatank/Documents/inter/spec3/testing'
testingDirList = os.listdir(testingPath)

# load testing
testingSpec = []
testingLabels = []

for dirName in testingDirList:
    tempFileList = os.listdir(f'{testingPath}/{dirName}')
    for fname in tempFileList:
        loaded = np.load(f'{testingPath}/{dirName}/{fname}').astype(np.single)
        loaded = loaded[..., np.newaxis]
        testingSpec.append(loaded)
        testingLabels.append(np.where(labels == dirName))

testingSpec = np.array(testingSpec)
testingLabels = np.array(testingLabels).flatten()

maxValue = np.amax(testingSpec)
minValue = np.amin(testingSpec)

testingSpec = (testingSpec - minValue) / (maxValue - minValue)

# testingDataset = tf.data.Dataset.from_tensor_slices((testingSpec, testingLabels))

model = tf.keras.models.load_model('models/9')

yPred = np.argmax(model.predict(testingSpec), axis=1)
yTrue = testingLabels

confusion_mtx = tf.math.confusion_matrix(yTrue, yPred)
plt.figure(figsize=(10, 8))
sns.heatmap(confusion_mtx,
            xticklabels=labels,
            yticklabels=labels,
            annot=True, fmt='g')
plt.title('Confusion Matrix')
plt.xlabel('Prediction')
plt.ylabel('Actual')
plt.show()
